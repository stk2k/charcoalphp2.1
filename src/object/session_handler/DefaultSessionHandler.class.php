<?php
/**
* デフォルトのセッションハンドラ実装
*
* PHP version 5
*
* @package    objects.session_handlers
* @author     CharcoalPHP Development Team
* @copyright  2008 stk2k, sazysoft
*/

class Charcoal_DefaultSessionHandler extends Charcoal_AbstractSessionHandler
{
    const TAG = "default_session_handler";

    private $save_path;

    /**
     *    constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Initialize instance
     *
     * @param array $config   configuration data
     */
    public function configure( $config )
    {
        parent::configure( $config );
    
        $event_stream = $this->getSandbox()->getCoreHookEventStream();
    
        $event_stream->push( 'session_handler.configure', $config, true );
    
        $config = new Charcoal_ConfigPropertySet($this->getSandbox()->getEnvironment(), $config);

        $session_name  = $config->getString( 'session_name', 'PHPSESSID' );
        $save_path     = $config->getString( 'save_path', '', TRUE );
        $lifetime      = $config->getInteger( 'lifetime', 0 );
        $valid_path    = $config->getString( 'valid_path', '' );
        $valid_domain  = $config->getString( 'valid_domain', '' );
        $ssl_only      = $config->getBoolean( 'ssl_only', FALSE );

        $save_path = us($save_path);
        $lifetime  = ui($lifetime);
        $ssl_only  = ub($ssl_only);
        $session_name  = us($session_name);
    
        // デフォルトのセッション保存先
        $event_stream->push( 'session_handler.get_save_path', $save_path, true );
        if ( !$save_path || !is_dir($save_path) ){
            log_warning( "session_handler", "session save path not exist: $save_path", self::TAG );
            $event_stream->push( 'session_handler.save_path_not_exist', $save_path, true );
            $save_path = Charcoal_ResourceLocator::getApplicationPath( 'sessions' );
        }

        // set session save path
        session_save_path( $save_path );
        $event_stream->push( 'session_handler.set_save_path', $save_path, true );
        
        // set session name
        session_name( $session_name );
        $event_stream->push( 'session_handler.set_session_name', $session_name, true );

        log_debug( "session_handler", "session_name:$session_name", self::TAG );
        log_debug( "session_handler", "save_path:$save_path", self::TAG );
        log_debug( "session_handler", "lifetime:$lifetime", self::TAG );
        log_debug( "session_handler", "valid_path:$valid_path", self::TAG );
        log_debug( "session_handler", "valid_domain:$valid_domain", self::TAG );
        log_debug( "session_handler", "ssl_only:$ssl_only", self::TAG );

        // メンバーに保存
        $this->save_path = $save_path;
    }

    /**
     * セッションファイルパスを取得
     *
     * @param int $id
     *
     * @return string
     */
    public function getSessionFile( $id )
    {
        return $this->save_path . "/sess_$id";
    }

    /**
     * open session
     *
     * @param string $save_path
     * @param string $session_name
     *
     * @return bool
     */
    public function open( $save_path, $session_name )
    {
        return true;
    }

    /**
     * close session
     */
    public function close()
    {
        return true;
    }

    /**
     * read session data
     *
     * @param int $id
     *
     * @return bool
     */
    public function read( $id )
    {
        $file = $this->getSessionFile( $id );
        if ( $this->getSandbox()->isDebug() ){
            log_info( "system,session_handler", "reading session file: $file", self::TAG );
        }

        if ( !is_readable($file) ){
            log_warning( "system,session_handler", "can't read session file[$file]", self::TAG );
            return '';
        }
        if ( $this->getSandbox()->isDebug() ){
            $sha1 = sha1_file($file);
            log_info( "system,session_handler", "sha1: $sha1", self::TAG );
        }

        $session_data = (string)@file_get_contents( $file );

        return  $session_data === FALSE ? '' : $session_data;
    }

    /**
     * write session data
     *
     * @param int $id
     * @param mixed $sess_data
     *
     * @return bool
     */
    public function write( $id, $sess_data )
    {
        $file = $this->getSessionFile( $id );

        $fp = @fopen($file,'w');
        if ( !$fp ){
            log_warning( "system,debug,error,session_handler", "fopen failed: $file", self::TAG );
            return false;
        }
        $write = fwrite($fp, $sess_data);
        fclose($fp);

        return $write !== FALSE ? TRUE : FALSE;
    }

    /**
     * destroy session
     *
     * @param int $id
     *
     * @return bool
     */
    public function destroy( $id )
    {
        $file = $this->getSessionFile( $id );

        return @unlink($file);
    }

    /**
     * garbage collection
     *
     * @param int $max_lifetime
     *
     * @return bool
     */
    public function gc( $max_lifetime )
    {
        if ( $dh = opendir($this->save_path) )
        {
            while( ($file = readdir($dh)) !== FALSE )
            {
                $file = $this->save_path . DIRECTORY_SEPARATOR . $file;
                if ( is_file($file) && filemtime($file) + $max_lifetime < time() ){
                    @unlink( $file );
                }
            }
            closedir($dh);
        }

        return true;
    }

}

