[![Latest Stable Version](https://poser.pugx.org/stk2k/charcoalphp2/v/stable.png)](https://packagist.org/packages/stk2k/charcoalphp2)

CharcoalPHP
=======

CharcoalPHP is NOT a MVC-based web framework but a flexible task oriented web framework.
It is very easy to write program. Deep knowledge about the framework helps you build
complex applications, but not required.

Framework principles
========

CharcoalPHP is designed by The '[SOLID][0]' principals.

- Single responsibility principle([SRP][1])
- Open/closed principle([OCP][2])
- Liskov substitution principle([LSP][3])
- Interface segregation principle([ISP][4])
- Dependency inversion principle([DIP][5])

[0]: http://en.wikipedia.org/wiki/SOLID_(object-oriented_design)
[1]: http://en.wikipedia.org/wiki/Single_responsibility_principle
[2]: http://en.wikipedia.org/wiki/Open/closed_principle
[3]: http://en.wikipedia.org/wiki/Liskov_substitution_principle
[4]: http://en.wikipedia.org/wiki/Interface_segregation_principle
[5]: http://en.wikipedia.org/wiki/Dependency_inversion_principle


Some Handy Links
----------------

[CharcoalPHP](http://charcoalphp.org) - Task oriented PHP Web Framework

